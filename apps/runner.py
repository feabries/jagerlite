from process import Driver
from utils import AsyncTimer


class Runner(object):
    STATUS_RUNNING = "running"
    STATUS_STOPPED = "stopped"

    COMMAND_STOP = "stop"

    def __init__(self, health_timer_period=3):
        self._health_timer_period = health_timer_period

        self._status = Runner.STATUS_STOPPED
        self._driver = Driver()
        self._health_timer = None
        self._func = None
        self._argv = None

    def _is_healthy(self):
        return self._driver.is_alive()

    def _assure_health(self):
        if not self._is_healthy():
            # Restart if the driver process is not healthy.
            self._cleanup_driver()
            self._setup_driver()

    def _setup_timer(self):
        if self._health_timer is None:
            self._health_timer = AsyncTimer(self._health_timer_period,
                                            self._assure_health)
            self._health_timer.start()

    def _cleanup_timer(self):
        if self._health_timer is not None:
            self._health_timer.cancel()
            self._health_timer = None

    def _setup_driver(self):
        assert self._func is not None
        assert self._argv is not None

        self._driver.start(self._func, *self._argv)

    def _cleanup_driver(self):
        self._driver.send(Runner.COMMAND_STOP)
        self._driver.terminate()

    def start(self, func, *argv):
        if self._status == Runner.STATUS_STOPPED:
            self._func = func
            self._argv = argv

            self._setup_driver()
            self._setup_timer()

            self._status = Runner.STATUS_RUNNING

    def stop(self):
        if self._status == Runner.STATUS_RUNNING:
            self._cleanup_timer()
            self._cleanup_driver()

        self._status = Runner.STATUS_STOPPED
