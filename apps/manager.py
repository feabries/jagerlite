import asyncio

from streaming import LiveStreamReader

from runner import Runner


def _run_app(signal, app_class, source):
    stream_reader = LiveStreamReader(source)
    stream_reader.open()

    app = app_class()

    while True:
        success, frame = stream_reader.read()

        if not success:
            break

        app.run([frame])

        if signal.poll():
            recvd_msg = signal.recv()
            if recvd_msg == Runner.COMMAND_STOP:
                break

    stream_reader.release()


class AppManager(object):

    def __init__(self, app_class, source):
        self._app_class = app_class
        self._source = source

        self._runner = Runner()

        self._io_loop = asyncio.get_event_loop()

    def start(self):
        self._runner.start(_run_app, self._app_class, self._source)

        self._io_loop.run_forever()
