from ctypes import c_bool
from ctypes import c_uint8
import time
from multiprocessing import Process
from multiprocessing import RawArray
from multiprocessing import Value

import cv2
import numpy as np

from jager.io.streaming import VideoFrame


class LiveStreamReader(object):

    def __init__(self,
                 source,
                 save=True,
                 save_path='/tmp/liveview.jpg',
                 save_size=(640, 480)):
        self._source = source
        self._save = save
        self._save_path = save_path
        self._save_size = save_size

        self._reader = None
        self._status = Value(c_bool, False)
        self._frame = None
        self._raw_frame = None
        self._timestamp = Value("d", 0)
        self._stopped = Value(c_bool, True)

    def __del__(self):
        if not self.is_stopped():
            self.release()

    def is_stopped(self):
        return self._stopped.value

    def is_started(self):
        return not self._stopped.value

    def open(self):
        assert not self.is_started(), 'Stream reader is already opened'

        self._stopped.value = False

        # Create a video capture
        reader = cv2.VideoCapture(self._source)
        reader.set(cv2.CAP_PROP_BUFFERSIZE, 0)
        status_value, self._frame = reader.read()

        assert status_value, \
               'Streaming source is not available: {}'.format(self._source)

        self._status.value = status_value

        shape = self._frame.shape
        self._raw_frame = RawArray(c_uint8, shape[0] * shape[1] * shape[2])
        self._raw_frame = np.frombuffer(self._raw_frame, dtype=np.uint8) \
                            .reshape(shape)

        self._timestamp.value = time.time()

        # Create a process to query frame looply
        process = Process(target=self._query_frame_loop,
                          args=(reader,
                                self._status,
                                self._raw_frame,
                                self._timestamp,
                                self._save,
                                self._save_path,
                                self._save_size,),
                          daemon=True)
        process.start()

    def release(self):
        assert not self.is_stopped(), 'Stream reader is already released'

        self._stopped.value = True

    def read(self):
        assert not self.is_stopped(), 'Stream reader must be opened'

        self._frame = np.array(self._raw_frame)

        video_frame = VideoFrame(self._frame, self._timestamp.value)

        return self._status.value, video_frame

    def _query_frame_loop(self,
                          reader,
                          status,
                          raw_frame,
                          timestamp,
                          save,
                          save_path,
                          save_size):
        while not self.is_stopped():
            status_value, frame = reader.read()
            status.value = status_value

            np.copyto(raw_frame, frame)

            timestamp.value = time.time()

            if save:
                resized = cv2.resize(frame, save_size)
                cv2.imwrite(save_path, resized)

        reader.release()
