import datetime
import random
import time

import cv2
import tensorflow as tf
import numpy as np

from jager.io.database import Database
from jager.io.notification import Notification
from jager.util.shape import BoundingBox
from jager.util.shape import Point
from jager.util import logging
from jager.util.generic import uuid

import detect_face
from face import crop_align_faces
from face_feature import FaceFeature
from robot_text import robot_text


RECOGNITION_THRESHOLD =0.3
OUTDATED_PERIOD = 3
SAME_FACE_REPEAT_THRESHOLD = 4
NUM_FEATURES_INSERT_DB = 3
NOTIFY_MIN_PERIOD = 10

RESIDENT_MINIMUM_FREQUENCY = 3
RESIDENT_LAST_DAYS = 7

FACE_COLLECTION_NAME = "reid.faces"

VISUALIZE = False
OUTPUT_VIDEO = False


class _FaceMatcher(object):

    def __init__(self, recog_thold):
        self._recog_thold = recog_thold

        self._detect_sess = tf.Session(graph=tf.Graph())
        tf.saved_model.loader.load(self._detect_sess,
                                   [tf.saved_model.tag_constants.SERVING],
                                   "models/mtcnn")

        self._pnet = lambda img : self._detect_sess.run(("pnet/conv4-2/BiasAdd:0", "pnet/prob1:0"), feed_dict={"pnet/input:0":img})
        self._rnet = lambda img : self._detect_sess.run(("rnet/conv5-2/conv5-2:0", "rnet/prob1:0"), feed_dict={"rnet/input:0":img})
        self._onet = lambda img : self._detect_sess.run(("onet/conv6-2/conv6-2:0", "onet/conv6-3/conv6-3:0", "onet/prob1:0"), feed_dict={"onet/input:0":img})

        self._minsize = 20 # minimum size of face
        self._threshold = [ 0.6, 0.7, 0.9 ]  # three steps's threshold
        self._factor = 0.709 # scale factor

        self._face_feature = FaceFeature("./models/mobilefacenet/MobileFaceNet_9925_9680.pb")

    def detect_faces(self, image, scale=0.5):
        input_width = int(image.shape[1] * scale)
        input_height = int(image.shape[0] * scale)
        input_image = cv2.resize(image, (input_width, input_height))

        locations, points = detect_face.detect_face(
            input_image,
            self._minsize,
            self._pnet,
            self._rnet,
            self._onet,
            self._threshold,
            self._factor,
        )

        locations = locations / scale
        points = points / scale

        bboxes = [BoundingBox(int(l[0]), int(l[1]), int(l[2]), int(l[3]))
                  for l in locations]

        landmarks = []
        for p in np.array(points).T:
            landmark = [Point(int(p[idx]), int(p[idx + 5]))
                        for idx in range(5)]
            landmarks.append(landmark)

        return bboxes, landmarks

    def featurize_faces(self, image, bboxes, landmarks):
        if not bboxes:
            return []

        aligned_faces = crop_align_faces(image, bboxes, landmarks)
        features = self._face_feature.get_features(aligned_faces)
        '''
        for aligned in aligned_faces:
            cv2.imwrite("aligned/{}.jpg".format(uuid()), aligned)
        '''

        return features

    def match(self, target_ids, target_features, face_feature):
        if not target_features:
            return None

        distances = []
        for target_feature in target_features:
            diff = np.subtract(target_feature, face_feature)
            dist = np.sqrt(np.sum(np.square(diff)))
            distances.append(dist)
        best_match_index = np.argmin(distances)

        if distances[best_match_index] <= self._recog_thold:
            face_id = target_ids[best_match_index]
        else:
            face_id = None

        return face_id


class _FaceDatabase(object):

    def __init__(self, db, matcher, after=None):
        self._db = db
        self._matcher = matcher
        self._after = after

        self._known_faces = self._init_known_faces(self._after)

    @property
    def ids(self):
        return self._known_faces["ids"]

    def _gen_id(self):
        return uuid()

    def _init_known_faces(self, after=None):
        filter = {}

        if after is not None:
            filter["timestamp"] = {
                "$gte": after,
            }

        faces = self._query_faces(filter=filter)

        known_faces = {
            "ids": [],
            "timestamps": [],
            "features": [],
        }
        for face in faces:
            known_faces["ids"].append(face["faceId"])
            known_faces["timestamps"].append(face["timestamp"])
            known_faces["features"].append(face["feature"])

        return known_faces

    def _query_faces(self, filter=None):
        return self._db.find(FACE_COLLECTION_NAME, filter=filter)

    def _insert_face(self, face_id, face_feature, timestamp):
        return self._db.insert_one(FACE_COLLECTION_NAME, {
            "faceId": face_id,
            "timestamp": timestamp,
            "feature": list(face_feature),
        })

    def _find_id_indexes(self, face_id):
        return np.array(self._known_faces["ids"]) == face_id

    def _select_by_indexes(self, lst, indexes):
        return np.array(lst)[indexes].tolist()

    def add(self, face_feature, timestamp, face_id=None):
        added_id = self._gen_id() if face_id is None else face_id

        self._insert_face(added_id, face_feature, timestamp)

        self._known_faces["ids"].append(added_id)
        self._known_faces["timestamps"].append(timestamp)
        self._known_faces["features"].append(face_feature)

        logging.debug("New face added: {}".format(added_id))

        return added_id

    def match(self, face_feature):
        return self._matcher.match(self._known_faces["ids"],
                                   self._known_faces["features"],
                                   face_feature)

    def get_freq(self, face_id, after=None):
        if face_id is None:
            return 0

        indexes =self._find_id_indexes(face_id)
        timestamps = np.array(self._known_faces["timestamps"])[indexes]

        if after is None:
            return timestamps.shape[0]
        else:
            return np.count_nonzero(timestamps >= after)



class _AppearanceTracker(object):

    def __init__(self, matcher, outdated_period):
        self._matcher = matcher
        self._outdated_period = outdated_period

        self._faces = {
            "ids": [],
            "features": [],
        }
        self._appearances = {}

    def _gen_id(self):
        return uuid()

    def _add_appearance(self,
                        added_id,
                        face_feature,
                        timestamp,
                        is_id_specified):
        self._faces["ids"].append(added_id)
        self._faces["features"].append(face_feature)

        self._appearances[added_id] = {
            "last_timestamp": timestamp,
            "is_id_specified": is_id_specified,
        }

    def _reserve_faces(self, reserved_indexes):
        self._faces["ids"] = self._select_by_indexes(
            self._faces["ids"],
            reserved_indexes,
        )
        self._faces["features"] = self._select_by_indexes(
            self._faces["features"],
            reserved_indexes,
        )

    def _find_id_indexes(self, face_id):
        return np.array(self._faces["ids"]) == face_id

    def _select_by_indexes(self, lst, indexes):
        return np.array(lst)[indexes].tolist()

    def update(self, face_feature, timestamp, face_id=None):
        if face_id is not None:
            added_id = face_id
            is_id_specified = True
        else:
            matched_id = self._matcher.match(self._faces["ids"],
                                             self._faces["features"],
                                             face_feature)

            added_id = self._gen_id() if matched_id is None else matched_id
            is_id_specified = False

        self._add_appearance(added_id, face_feature, timestamp, is_id_specified)

    def clear_outdated(self, cur_timestamp):
        reserved_indexes = np.ones(len(self._faces["ids"]), dtype=bool)
        outdated_list = []

        for face_id, appearance in list(self._appearances.items()):
            elapsed = cur_timestamp - appearance["last_timestamp"]
            if elapsed > self._outdated_period:
                del self._appearances[face_id]
                indexes = self._find_id_indexes(face_id)
                reserved_indexes = np.logical_xor(reserved_indexes, indexes)
                outdated_list.append({
                    "id": face_id if appearance["is_id_specified"] else None,
                    "features": self._select_by_indexes(self._faces["features"],
                                                        indexes),
                    "timestamp": appearance["last_timestamp"],
                })

        self._reserve_faces(reserved_indexes)

        return outdated_list


class _SentenceSampler(object):

    def __init__(self, text_db):
        self._text_db = text_db

    def _current_hour(self):
        return datetime.datetime.now().hour

    def sample(self, set_name):
        hour = self._current_hour()

        sentences = None
        for text_cand in self._text_db[set_name]:
            if hour >= text_cand['from'] and hour < text_cand['to']:
                sentences = text_cand['sentences']
                break

        assert sentences is not None, 'No matched hour range: {}'.format(hour)

        return random.choice(sentences)


class FaceReIdentification(object):

    def __init__(self):
        self._recog_thold = RECOGNITION_THRESHOLD
        self._outdated_period = OUTDATED_PERIOD
        self._repeat_thold = SAME_FACE_REPEAT_THRESHOLD
        self._num_inserted = NUM_FEATURES_INSERT_DB
        self._notify_min_period = NOTIFY_MIN_PERIOD

        self._resident_min_frequency = \
            NUM_FEATURES_INSERT_DB * RESIDENT_MINIMUM_FREQUENCY
        self._resident_last_seconds = \
            RESIDENT_LAST_DAYS * 24 * 60 * 60

        self._db = Database()

        self._notification = Notification()
        self._last_notify_timestamp = -1

        face_after = time.time() - self._resident_last_seconds

        self._matcher = _FaceMatcher(self._recog_thold)
        self._face_db = _FaceDatabase(self._db, self._matcher, after=face_after)
        self._appearance_tracker = _AppearanceTracker(self._matcher,
                                                      self._outdated_period)
        self._sentence_sampler = _SentenceSampler(robot_text)

    def _draw(self, image, face_ids, bboxes, names):
        drawn = image.copy()

        for idx, face_id in enumerate(face_ids):
            cv2.putText(drawn,
                        face_id,
                        (30, 30 + idx * 20),
                        cv2.FONT_HERSHEY_DUPLEX,
                        0.5,
                        (213, 33, 209),
                        1)

        for bbox, name in zip(bboxes, names):
            cv2.rectangle(drawn,
                          (bbox.xmin, bbox.ymin),
                          (bbox.xmax, bbox.ymax),
                          (0, 0, 255),
                          2)
            cv2.rectangle(drawn,
                          (bbox.xmin, bbox.ymax - 35),
                          (bbox.xmax, bbox.ymax),
                          (0, 0, 255),
                          cv2.FILLED)
            cv2.putText(drawn,
                        name,
                        (bbox.xmin + 6, bbox.ymax - 6),
                        cv2.FONT_HERSHEY_DUPLEX,
                        1.0,
                        (255, 255, 255),
                        1)

        return drawn

    def _insert_db(self, appearance):
        # TODO(feabries su): Use a better policy to choose features,
        # instead of choosing features randomly.
        indexes = np.random.choice(len(appearance["features"]),
                                   size=self._num_inserted)

        added_id = self._face_db.add(appearance["features"][indexes[0]],
                                     appearance["timestamp"],
                                     face_id=appearance["id"])

        for index in indexes[1:]:
            self._face_db.add(appearance["features"][index],
                              appearance["timestamp"],
                              face_id=added_id)

    def _contains_resident(self, face_ids, timestamp):
        after = time.time() - self._resident_last_seconds

        for face_id in face_ids:
            freq = self._face_db.get_freq(face_id, after=after)
            if freq >= self._resident_min_frequency:
                return True

        return False

    def run(self, frames):
        for frame in frames:
            image = frame.image
            timestamp = frame.timestamp

            face_bboxes, landmarks = self._matcher.detect_faces(image)
            face_features = self._matcher.featurize_faces(image,
                                                          face_bboxes,
                                                          landmarks)

            matched_ids = []
            for face_feature in face_features:
                matched_id = self._face_db.match(face_feature)
                self._appearance_tracker.update(face_feature,
                                                timestamp,
                                                face_id=matched_id)

                matched_ids.append(matched_id)

            if matched_ids:
                last_notify_duration = timestamp - self._last_notify_timestamp
                if last_notify_duration > self._notify_min_period:
                    contained = self._contains_resident(matched_ids, timestamp)
                    set_name = "resident" if contained else "stranger"
                    sentence = self._sentence_sampler.sample(set_name)
                    self._notification.notify_sota(sentence)
                    self._last_notify_timestamp = timestamp

            outdated_list = self._appearance_tracker.clear_outdated(timestamp)
            for outdated in outdated_list:
                if len(outdated["features"]) >= self._repeat_thold:
                    self._insert_db(outdated)

            if VISUALIZE or OUTPUT_VIDEO:
                names = ["Unkown" if m is None else m for m in matched_ids]
                face_ids = self._face_db.ids
                drawn = self._draw(image, face_ids, face_bboxes, names)

            if VISUALIZE:
                cv2.imshow("Display", drawn)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break

            if OUTPUT_VIDEO:
                if not hasattr(self, "_video_writer"):
                    self._video_writer = cv2.VideoWriter(
                        "output.avi",
                        cv2.VideoWriter_fourcc(*"XVID"),
                        20,
                        (image.shape[1], image.shape[0]),
                    )

                self._video_writer.write(drawn)
