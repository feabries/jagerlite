import multiprocessing
from multiprocessing import Pipe


class Driver(object):

    def __init__(self):
        self._driver_process = None
        self._sig_parent = None
        self._sig_child = None

    def start(self, func, *argv):
        self._sig_parent, self._sig_child = Pipe()
        self._driver_process = multiprocessing.Process(
            target=Driver.run_driver_func,
            args=(func,
                  self._sig_child,
                  *argv))
        self._driver_process.start()

    def terminate(self, timeout=5):
        assert self._driver_process is not None, "It's an error to attempt to \
            terminate a driver before it has been started."
        try:
            self._driver_process.join(timeout)
        except TimeoutError:
            logging.error("The driver was not terminated for some reason "
                          "(exitcode: {}), force to terminate it."
                          .format(self._driver_process.exitcode))
            self._driver_process.terminate()
            time.sleep(0.1)
        finally:
            self._sig_parent.close()
            self._sig_parent = None
            self._sig_child = None
            self._driver_process = None

    def poll(self, timeout=None):
        if self._sig_parent is not None:
            if timeout is not None:
                return self._sig_parent.poll(timeout)
            else:
                return self._sig_parent.poll()
        else:
            return False

    def send(self, msg):
        self._sig_parent.send(msg)

    def recv(self):
        return self._sig_parent.recv()

    def is_alive(self):
        if self._driver_process is None:
            return False

        return self._driver_process.is_alive()

    @staticmethod
    def run_driver_func(driver_func, signal, *argv):
        try:
            driver_func(signal, *argv)
        finally:
            signal.close()
