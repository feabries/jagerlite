from manager import AppManager
from face_reid import FaceReIdentification


STREAM_SOURCE = 0


def main():
    # TODO(feabries su): Read configurations from the database or file, instead
    # of hard coding.
    manager = AppManager(FaceReIdentification, STREAM_SOURCE)

    manager.start()


if __name__ == "__main__":
    main()
