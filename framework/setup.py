import os
from shutil import copy


# Metadata about the package
NAME = 'jager'


def cp_static_files(static_files):
    """Copy files to static folder.

    Args:
      static_files (list of string): The files to be copied.
    """
    static_dir = os.path.join(NAME, 'static')
    # Create the static directory if necessary.
    if not os.path.exists(static_dir):
        os.makedirs(static_dir)
    # Now, copy files into the static directory.
    for static_file in static_files:
        if os.path.isfile(static_file):
            copy(static_file, static_dir)
            print('Copy {} into {}'.format(static_file, static_dir))


def main():
    cp_static_files([
        '../shared/messaging.json',
    ])

    # TODO(feabries su): Implement setup scripts.


if __name__ == '__main__':
    main()
