import mongoengine as mongo
from pymongo.errors import PyMongoError

from jager.errors.errors import DatabaseError


def connect_db(db_name, host=None, authentication_source=None):
    """Connect to a MongoDB database.

    Args:
        db_name (string): The database name for connection.
        host (string): The host URL for connection. Defaults to None.
        authentication_source (string): Database to authenticate against.
            Defaults to None.

    Returns:
        (`pymongo.MongoClient`, `pymongo.database.Database`): The MongoDB
            client and database instance.
    """
    try:
        client = mongo.connect(db=db_name,
                               host=host,
                               authentication_source=authentication_source)
        db = mongo.connection.get_db()
    except PyMongoError as e:
        raise DatabaseError(e)
    except mongo.MongoEngineConnectionError as e:
        raise DatabaseError(e)

    return client, db
