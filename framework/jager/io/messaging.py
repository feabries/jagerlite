import asyncio
import json

from nats.aio.client import Client as NATS
from nats.aio.errors import ErrConnectionClosed
from nats.aio.errors import ErrTimeout

from jager import logging

class MessagingClient(object):
    def __init__(self, servers):
        self._servers = servers

        self._cli = NATS()
        self._io_loop = asyncio.get_event_loop()

    def _check_connected(self):
        assert self._cli.is_connected, "Messaging is not connected"

    def _check_disconnected(self):
        assert not self._cli.is_connected, "Messaging is connected"

    async def _connect(self):
        options = {
            "servers": self._servers,
            "io_loop": self._io_loop,
            "max_reconnect_attempts": -1,
            "reconnect_time_wait": 1,
            "disconnected_cb": self._on_disconnected,
            "reconnected_cb": self._on_reconnected,
            "error_cb": self._on_error,
            "closed_cb": self._on_closed,
        }

        try:
            await self._cli.connect(**options)
            logging.info("Messaging connected")
        except Exception as e:
            logging.exception(e)
            raise

    def _disconnect(self):
        logging.info("Destroying messaging")
        self._cli.close()

    async def _on_disconnected(self):
        logging.info("Messaging disconnected")

    async def _on_reconnected(self):
        logging.info("Messaging reconnected")

    async def _on_error(self, e):
        logging.error("Messaging ERROR: {}".format(e))

    async def _on_closed(self):
        logging.info("Messaging connection is closed")

    def __del__(self):
        if self._cli.is_connected:
            self._disconnect()

    async def connect(self):
        self._check_disconnected()

        await self._connect()

    def disconnect(self):
        self._check_connected()

        self._disconnect()

    async def publish(self, channel, msg):
        self._check_connected()

        logging.debug("try to publish, channel: {}, msg: {}"
                      .format(channel, msg))

        try:
            await self._cli.publish(channel, json.dumps(msg).encode())
            logging.debug("Success to publish, channel: {}, msg: {}"
                          .format(channel, msg))
        except ErrConnectionClosed:
            logging.error("Connection closed prematurely")
            raise
        except ErrTimeout:
            logging.error("Timeout occurred when publishing")
            raise
        except Exception as e:
            logging.error("Failed to publish: {}".format(e))
            raise

    # TODO(feabries su): There are more and more messaging methods, please
    # implement them when they are needed.
