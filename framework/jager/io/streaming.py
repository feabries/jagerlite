"""Classes for Streaming I/O."""

import os
import signal
import time
import threading
from queue import Queue
from collections import deque
from urllib.parse import urlparse

import cv2
import numpy as np

from jager.util import logging


DEFAULT_STREAM_BUFFER_SIZE = 64     # frames

# The default threshold full percent of buffer is 70%
DEFAULT_STREAM_BUFFER_THRES_PERCENT = 70
DEFAULT_FPS = 15


class ConnectionError(Exception):
    pass


class EndOfVideoError(Exception):
    pass


class TaskTimeoutError(Exception):
    pass


def _is_livestream(url):
    """Check whether the source is a livestream or not.

    We check the protocol of the source url to determine whether
    it's a livestream, since currently we only support "RTSP",
    source url which not starts with "rtsp://" would be consider
    as not an livestream source.

    Returns:
        True if it's a livestream source and false otherwise.
    """
    # XXX: Not a very robust way for checking the source protocol.
    return urlparse(url).scheme.lower() == "rtsp"


def run_with_timeout(timeout, func, *args, **kwargs):
    """Execute a task with a timeout.

    Args:
        timeout (float): The timeout value.
        func (function): The task function to be executed.

    Returns:
        The task result.

    Raises:
        An TaskTimeoutError exception if the execution of the task exceeded
        the timeout, otherwise, raises the task exceptions, if any.
    """

    class Job(threading.Thread):
        def __init__(self, func, *args, **kwargs):
            threading.Thread.__init__(self)
            self.daemon = True
            self._func = func
            self._args = args
            self._kwargs = kwargs
            self.result = None
            self.exception = None

        def run(self):
            try:
                self.result = self._func(*self._args, **self._kwargs)
            except Exception as e:
                self.exception = e

        def terminate(self):
            signal.pthread_kill(self.ident, 0)

    job = Job(func, *args, **kwargs)
    job.start()
    job.join(timeout=timeout)

    if job.is_alive():
        # job's timeout has happened, cancel it and notify the caller
        job.terminate()
        raise TaskTimeoutError("The task has exceeded the timeout.")
    elif job.exception is not None:
        raise job.exception
    else:
        return job.result


class VideoFrame(object):
    def __init__(self, image_raw, timestamp=None):
        self.image = image_raw
        if timestamp is None:
            self.timestamp = time.time()
        else:
            self.timestamp = timestamp


class StreamReaderThread(threading.Thread):
    def __init__(self,
                 reader,
                 queue,
                 width,
                 height,
                 src,
                 timeout,
                 stop_event,
                 cap_interval,
                 is_livestream):
        super(StreamReaderThread, self).__init__()
        self._reader = reader
        self._queue = queue
        self._stop_event = stop_event
        self._cap_interval = cap_interval / 1000.0
        self._is_livestream = is_livestream
        self._exception = None
        self._blankframe = np.zeros((height, width, 3))
        self._src = src
        self._timeout = timeout
        self._is_retrying = False
        self._retry_thread = None

    def run(self):
        while not self._stop_event.is_set():
            success, image = self._reader.read()
            timestamp = time.time()
            frame = VideoFrame(self._blankframe, timestamp)
            if not success:
                if self._is_livestream:
                    self._exception = \
                        VideoStreamReaderBasic.ERROR_CONNECTION
                else:
                    self._exception = \
                        VideoStreamReaderBasic.ERROR_ENDOFVIDEO
                # retry opening stream if not success
                if not self._is_retrying:
                    self._is_retrying = True
                    self._retry_thread = threading.Thread(target=self._retry)
                    self._retry_thread.daemon = True
                    self._retry_thread.start()
            else:
                frame.image = image
                self._exception = None
            self._queue.appendleft(frame)
            time.sleep(self._cap_interval)
        logging.info("Reader thread is terminated")

    def _retry(self):
        try:
            run_with_timeout(self._timeout, self._reader.open, self._src)
        except:
            pass
        finally:
            self._is_retrying = False

    def get_exception(self):
        return self._exception


class VideoStreamReaderBasic(object):
    STATUS_SRC_UP = 'up'
    STATUS_SRC_DOWN = 'down'
    ERROR_CONNECTION = ''
    ERROR_ENDOFVIDEO = ''
    ERROR_TASKTIMEOUTERROR = ''
    # TODO add abc class VideoStreamReader to inherit


class VideoSyncMultiStreamReader(object):
    """The synchronized multiple video stream reader.

    First this create reader for every video stream source.
    The reader to read frames from a video stream source. The source can be a
    video file or a live stream such as RTSP, Motion JPEG. Each captured frame
    will be stored as a VideoFrame object with an image tensor and the
    timestamp of which the frame been captured.

    There is another thread keep checking sync frames from different video
    stream sources.

    The "image" tensor is a 3-dimensional numpy `ndarray` whose type is uint8
    and the shape format is:
    1. Image height.
    2. Image width.
    3. Number of channels, which is usually 3.

    The "timestamp" tensor is a 0-dimensional numpy `ndarray` whose type is
    string.
    """

    def __init__(self, src, buffer_size=DEFAULT_STREAM_BUFFER_SIZE):
        """Initialize a `VideoSyncMultiStreamReader` object.

        Args:
            src (string): video stream source
            buffer_size (int): The maximum size to buffering the video stream.
        """

        self._src = src
        self._num_source = len(self._src)
        self._status_source = [VideoStreamReaderBasic.STATUS_SRC_DOWN
                                for _ in range(self._num_source)]
        self._readers = [cv2.VideoCapture()
                            for _ in range(self._num_source)]
        self._thread = [None for _ in range(self._num_source)]
        self._sync_thread = None
        self._sync_queue  = [deque(maxlen=buffer_size)
                            for _ in range(self._num_source)]
        self._sync_wait_time = 0.01
        self._frame_queue = [deque(maxlen=buffer_size)
                                for _ in range(self._num_source)]
        self._stop_event = [threading.Event()
                            for _ in range(self._num_source)]
        self._video_info = [{} for _ in range(self._num_source)]
    def open(self, timeout=15, fps=DEFAULT_FPS):
        """Open video streaming sources and create sync thread

        Args:
            timeout (int): open streaming connection timeout
            fps (int): streaming fps
        Returns:
            (list of string): video sources status
            (string): video stream error
        """
        # init status
        self._status_source = [VideoStreamReaderBasic.STATUS_SRC_DOWN
                                for i in range(self._num_source)]
        for i in range(self._num_source):
            logging.info("Opening video source: {}".format(self._src[i]))
            if self._readers[i].isOpened():
                self._status_source[i] = VideoStreamReaderBasic.STATUS_SRC_DOWN
                return self._status_source, \
                        VideoStreamReaderBasic.ERROR_CONNECTION

        # Open video source
        for i in range(self._num_source):
            try:
                run_with_timeout(timeout, self._readers[i].open, self._src[i])
            except:
                logging.error("Error occurred when opening video source"
                              " from {}".format(self._src[i]))
                self._status_source[i] = VideoStreamReaderBasic.STATUS_SRC_DOWN
                return self._status_source, \
                        VideoStreamReaderBasic.ERROR_CONNECTION

        capture_interval = 1000.0 / fps
        for i in range(self._num_source):
            # Get video information
            success, image = self._readers[i].read()
            if not success:
                self._status_source[i] = \
                    VideoStreamReaderBasic.STATUS_SRC_DOWN
                return self._status_source, \
                        VideoStreamReaderBasic.ERROR_CONNECTION
            self._status_source[i] = VideoStreamReaderBasic.STATUS_SRC_UP

            height, width, _ = image.shape
            self._video_info[i]["frame_size"] = (width, height)
            # Start reader thread
            self._stop_event[i].clear()
            logging.info("Starting reader thread")
            self._thread[i] = StreamReaderThread(self._readers[i],
                                              self._frame_queue[i],
                                              width,
                                              height,
                                              self._src[i],
                                              timeout,
                                              self._stop_event[i],
                                              capture_interval,
                                              _is_livestream(self._src[i]))
        for i in range(self._num_source):
            self._thread[i].daemon = True
            self._thread[i].start()
        self._sync_thread = threading.Thread(target=self._sync, args=(fps, ))
        self._sync_thread.daemon = True
        self._sync_thread.start()

        return self._status_source, None

    def release(self):
        for i in range(self._num_source):
            self._stop_event[i].set()
            if hasattr(self, "_thread"):
                self._thread[i].join()
            self._readers[i].release()
            self._frame_queue[i].clear()
        self._sync_thread.join()
        self._sync_queue.clear()

    def get_video_info(self):
        # TODO update this when developing stitching pipeline check
        return self._video_info[0]

    def _read_all(self):
        return [[self._sync_queue[i].pop()
                    for _ in range(len(self._sync_queue[i]))]
                    for i in range(len(self._sync_queue))]

    def _read(self, batch_size):
        return [[self._sync_queue[i].pop()
                    for _ in range(batch_size)]
                    for i in range(len(self._sync_queue))]

    def is_busy(self):
        full_percent = sum([len(self._sync_queue[i])
                        for i in range(len(self._sync_queue))]) / self._num_source /\
                        DEFAULT_STREAM_BUFFER_SIZE * 100
        return full_percent > DEFAULT_STREAM_BUFFER_THRES_PERCENT

    def read(self, batch_size=1):
        """The routine of synced multi video stream frames capturer capturation

        Returns:
            (list of list of `VideoFrame`): A list of synced VideoFrame objects
                from every streaming source. The length of the list is
                determined by the `len(_num_source)`, The second dimension
                length of the list is  `batch_size`.
            (list of string): video sources status
            (string): video stream error
        """
        cur_q_size = min([len(self._sync_queue[i])
                        for i in range(self._num_source)])

        while cur_q_size < batch_size:
            time.sleep(self._sync_wait_time)
            cur_q_size = min([len(self._sync_queue[i])
                            for i in range(self._num_source)])

        self._status_source = [VideoStreamReaderBasic.STATUS_SRC_UP
                                for i in range(self._num_source)]
        count_err = 0
        for i in range(self._num_source):
            if self._thread[i].get_exception() is not None:
                self._status_source[i] = VideoStreamReaderBasic.STATUS_SRC_DOWN
                count_err += 1
        if count_err == self._num_source:
            return [], self._status_source, \
                    VideoStreamReaderBasic.ERROR_CONNECTION
        data = self._read(batch_size)
        return data, self._status_source, None

    def _newest_timestamp(self, frames):
        max_ts_id = 0
        max_ts = 0
        for i in range(len(frames)):
            if frames[i][1].timestamp > max_ts:
                max_ts = frames[i][1].timestamp
                max_ts_id = i
        return (max_ts_id, max_ts)

    def _sync(self, fps):
        """Keep sync frame in queue from different sources"""
        sync_interval = 2 / fps
        count = 0
        while True:
            # check if queue is empty
            for i in range(self._num_source):
                while len(self._frame_queue[i]) == 0:
                    time.sleep(self._sync_wait_time)
            # try syncing frames
            frames = []
            cam_id_throw_frame = [i for i in range(self._num_source)]
            while True:
                # get last item from each queue
                for i in cam_id_throw_frame:
                    while True:
                        if len(self._frame_queue[i]) > 0:
                            break
                        time.sleep(self._sync_wait_time)
                    frame = self._frame_queue[i].pop()
                    frames.append((i, frame))

                # find the newest timestamp
                max_ts_id, max_ts = self._newest_timestamp(frames)

                cam_id_throw_frame = []
                drop_index = []
                exist_empty_queue = False
                all_queue_empty = True
                all_frames_synced = True
                for i in range(len(frames)):
                    all_queue_empty &= len(self._frame_queue[i]) == 0
                    exist_empty_queue |= len(self._frame_queue[i]) == 0
                    if max_ts - frames[i][1].timestamp > sync_interval:
                        if len(self._frame_queue[i]) > 0:
                            cam_id_throw_frame.append(i)
                            drop_index.append(i)
                        all_frames_synced = False
                if ( all_frames_synced or
                         all_queue_empty or
                         len(self._frame_queue[max_ts_id]) > 0 and exist_empty_queue ):
                    break

                # drop all item that is not in sync interval
                frames = [frames[i] for i in range(len(frames)) if i not in drop_index]

            # sort according to cam id
            frames = sorted(frames, key=lambda x: x[0])
            for i in range(len(frames)):
                self._sync_queue[frames[i][0]].appendleft(frames[i][1])

            # available at the time
            time.sleep(self._sync_wait_time)


class VideoStreamReader(object):
    """The video stream reader.

    The reader to read frames from a video stream source. The source can be a
    video file or a live stream such as RTSP, Motion JPEG. Each captured frame
    will be stored as a VideoFrame object with an image tensor and the
    timestamp of which the frame been captured.

    The "image" tensor is a 3-dimensional numpy `ndarray` whose type is uint8
    and the shape format is:
    1. Image height.
    2. Image width.
    3. Number of channels, which is usually 3.

    The "timestamp" tensor is a 0-dimensional numpy `ndarray` whose type is
    string.
    """

    def __init__(self, src, buffer_size=DEFAULT_STREAM_BUFFER_SIZE):
        """Initialize a `VideoStreamReader` object.

        Args:
            src (string): video stream source
            buffer_size (int): The maximum size to buffering the video stream.
        """
        self._src = src
        self._status_source = VideoStreamReaderBasic.STATUS_SRC_DOWN
        self._reader = cv2.VideoCapture()
        self._queue = deque(maxlen=buffer_size)
        self._stop_event = threading.Event()
        self._video_info = {}

    def open(self, timeout=15, fps=DEFAULT_FPS):
        """Open video streaming source

        Args:
            timeout (int): open streaming connection timeout
            fps (int): streaming fps
        Returns:
            (string): video source status
            (string): video stream error
        """
        logging.info("Opening video source: {}".format(self._src))

        self._status_source = VideoStreamReaderBasic.STATUS_SRC_DOWN
        if self._reader.isOpened():
            return self._status_source, \
                    VideoStreamReaderBasic.ERROR_CONNECTION

        # Open video source
        try:
            run_with_timeout(timeout, self._reader.open, self._src)
        except:
            logging.error("Error occurred when opening video source"
                          " from {}".format(self._src))
            return self._status_source, \
                    VideoStreamReaderBasic.ERROR_CONNECTION

        # Get video information
        success, image = self._reader.read()
        if not success:
            return self._status_source, \
                    VideoStreamReaderBasic.ERROR_CONNECTION
        self._status_source = VideoStreamReaderBasic.STATUS_SRC_UP

        height, width, _ = image.shape
        self._video_info["frame_size"] = (width, height)

        # Start reader thread
        self._stop_event.clear()
        capture_interval = 1000.0 / fps
        logging.info("Starting reader thread")
        self._thread = StreamReaderThread(self._reader,
                                          self._queue,
                                          width,
                                          height,
                                          self._src,
                                          timeout,
                                          self._stop_event,
                                          capture_interval,
                                          _is_livestream(self._src))
        self._thread.daemon = True
        self._thread.start()

        return self._status_source, None

    def release(self):
        self._stop_event.set()
        if hasattr(self, "_thread"):
            self._thread.join()
        self._reader.release()
        self._queue.clear()

    def get_video_info(self):
        return self._video_info

    def _read_all(self):
        return [self._queue.pop() for _ in range(len(self._queue))]

    def _read(self, batch_size):
        return [self._queue.pop() for _ in range(batch_size)]

    def is_busy(self):
        full_percent = (len(self._queue) / DEFAULT_STREAM_BUFFER_SIZE)*100
        return full_percent > DEFAULT_STREAM_BUFFER_THRES_PERCENT

    def read(self, batch_size=1):
        """The routine of video stream capturer capturation.

        Returns:
            (list of `VideoFrame`): A list of captured VideoFrame objects.
                The length of the list is determined by the `batch_size`.
            (string): video source status
            (string): video stream error
        """
        cur_q_size = len(self._queue)
        while cur_q_size < batch_size:
            time.sleep(0.1)
            cur_q_size = len(self._queue)

        self._status_source = VideoStreamReaderBasic.STATUS_SRC_UP
        err = self._thread.get_exception()
        if err is not None:
            self._status_source = VideoStreamReaderBasic.STATUS_SRC_DOWN
            return [], self._status_source, err

        data = self._read(batch_size)
        return data, self._status_source, None


class StreamWriterThread(threading.Thread):
    def __init__(self, writer, queue, stop_event):
        super(StreamWriterThread, self).__init__()
        self._writer = writer
        self._queue = queue
        self._stop_event = stop_event
        self._exception = None

    def run(self):
        try:
            while True:
                if self._queue.empty():
                    if self._stop_event.is_set():
                        break
                    else:
                        time.sleep(0.01)
                        continue
                frame = self._queue.get()
                self._writer.write(frame.image)
                self._queue.task_done()
            logging.info("Writer thread is terminated")
        except Exception as e:
            logging.error(str(e))
            self._exception = e

    def get_exception(self):
        return self._exception


class VideoStreamWriter(object):
    def __init__(self):
        self._writer = cv2.VideoWriter()
        self._queue = Queue()
        self._stop_event = threading.Event()

    def open(self, filename, fps, size):
        if self._writer.isOpened():
            raise RuntimeError("Stream is already opened")

        _, ext = os.path.splitext(filename)
        if ext == ".mp4":
            # Use I420, which is the most common YUV420p foramt, to make the
            # generated videos widely supported.
            # (If we don't specify, the output format may be YUV444p, which is
            #  not supported by some browsers on mobile phones)
            filename = ('appsrc ! videoconvert ! video/x-raw,format=(string)I420 ! x264enc ! mp4mux !'
                         ' filesink location={}'.format(filename))
            fourcc = 0
        else:
            fourcc = cv2.VideoWriter_fourcc(*'XVID')

        if not self._writer.open(filename, fourcc, fps, size):
            raise RuntimeError("Can't open video file {}"
                               .format(filename))

        self._stop_event.clear()

        logging.info("Starting writer thread")
        self._thread = StreamWriterThread(self._writer,
                                          self._queue,
                                          self._stop_event)
        self._thread.daemon = True
        self._thread.start()

    def end(self):
        try:
            self._stop_event.set()
            if hasattr(self, "_thread") and self._thread.is_alive():
                if not self._queue.empty():
                    self._queue.join()
                self._thread.join()
            self._writer.release()
        except Exception as e:
            logging.error(str(e))

    def write(self, frames):
        if isinstance(frames, list):
            for frame in frames:
                self._queue.put(frame)
        else:
            self._queue.put(frames)
