import asyncio

from jager.errors.errors import NotificationCommandError
from jager.io.messaging import MessagingClient
from jager.util.generic import get_messaging_config


class Notification(object):

    def __init__(self, msg_servers=None):
        # TODO(feabries su): Read configurations from the file, instead of hard
        # coding.
        self._msg_servers = msg_servers \
                            if msg_servers is not None \
                            else ["nats://localhost:4222"]

        msg_config = get_messaging_config()
        self._notif_cmds = msg_config["ch_notification"]
        self._notif_ch = msg_config["channels"]["NOTIFICATION"]

        self._msg_cli = MessagingClient(self._msg_servers)
        self._io_loop = asyncio.get_event_loop()

        self._io_loop.run_until_complete(self._msg_cli.connect())

    def __del__(self):
        self._msg_cli.disconnect()

    def notify(self, cmd, params):
        if cmd not in self._notif_cmds.keys():
            raise NotificationCommandError(
                "Non-Supported notification command: {}".format(cmd),
            )

        self._io_loop.run_until_complete(self._msg_cli.publish(self._notif_ch, {
            "command": self._notif_cmds[cmd],
            "params": params,
        }))

    def notify_sota(self, text):
        self.notify("SOTA_ROBOT", {
            "text": text,
        })
