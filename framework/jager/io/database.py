import urllib

from jager.errors.errors import DatabaseError
from jager.io.db_engine import connect_db
from jager.util import logging


class Database(object):

    def __init__(self, db_name=None, host=None, authentication_source="admin"):
        # TODO(feabries su): Read configurations from the file, instead of hard
        # coding.
        self._db_name = db_name if db_name is not None else "jager_production"
        self._host = host \
                     if host is not None \
                     else "mongodb://jager:{}@localhost:21034" \
                          .format(urllib.parse.quote("jager@0402!prod"))
        self._authentication_source = authentication_source

        try:
            self._client, self._db = connect_db(
                self._db_name,
                host=self._host,
                authentication_source=self._authentication_source,
            )
            logging.info("Database connected")
        except DatabaseError as e:
            logging.error("Database error occured while connecting: {}"
                          .format(str(e)))
            raise

    def insert_one(self, collection_name, doc):
        inserted_id = self._db[collection_name].insert_one(doc).inserted_id

        return inserted_id

    def find(self, collection_name, filter=None):
        return list(self._db[collection_name].find(filter=filter))

    # TODO(feabries su): There are more and more database methods, please
    # implement them when they are necessary.
