class NotificationCommandError(Exception):
    pass


class DatabaseError(Exception):
    pass
