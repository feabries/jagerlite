"""Generic utilities."""

import json
import os
from uuid import uuid4

import jager


def get_static_path(file_name):
    """Get path of a static file.

    file_name (string): The static file name.
    """
    file_dir = os.path.join(os.path.dirname(jager.__file__), "static")
    file_path = os.path.join(file_dir, file_name)

    return file_path


def get_messaging_config(messaging_file="messaging.json"):
    """Get the messaging format file

    messaging_file (string): The path to the messaging format file. Defaults to
      "messaging.json".
    """
    with open(get_static_path(messaging_file), "r") as f:
        return json.load(f)


def uuid():
    """Generate a UUID.

    Returns:
        string: The generated UUID.
    """
    return str(uuid4())
