// FIXME(feabries su): The code is so ugly, please help refactor it.

const fs = require('fs');

const async = require('async');
const WebSocket = require('ws');

// TODO(feabries su): Read from the configuration file instead of hard coding.
const PORT = 4897;
const PATH = '/liveview';
const IMAGE_PATH = '/tmp/liveview.jpg';

function start(
    port,
    path,
    imagePath,
    sendImageInterval = 100,
    websocketKeepAliveInterval = 10000,
) {
    const wsServer = new WebSocket.Server({
        port,
        path,
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
    });

    wsServer.on('connection', ws => {
        console.log('Liveview: WebSocket connected');

        ws.isAlive = true;
        ws.on('pong', () => {
            ws.isAlive = true;
        });
        ws.on('close', () => {
            console.log('Liveview: WebSocket closed');
        });
        ws.on('error', err => {
            console.error(err);
        });
    });
    wsServer.on('error', err => {
        console.error(err);
    });

    setInterval(() => {
        wsServer.clients.forEach(client => {
            if (client.isAlive === false) {
                return client.terminate();
            }
            client.isAlive = false;
            client.ping('', false, true);
        });
    }, websocketKeepAliveInterval);

    setInterval(() => {
        fs.readFile(imagePath, (err, bitmap) => {
            if (err) {
                console.error(err);
            } else {
                const image = Buffer.from(bitmap).toString('base64');

                async.each(
                    wsServer.clients,
                    (client, callback) => {
                        client.send(image, err => {
                            if (err) {
                                return callback(err);
                            }
                            callback();
                        });
                    },
                    err => {
                        if (err) {
                            console.error(err);
                        }
                    },
                );
            }
        });
    }, sendImageInterval);
}

start(PORT, PATH, IMAGE_PATH);
