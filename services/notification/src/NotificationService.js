const forEach = require('lodash/forEach');

const models = require('./models');
const { MessagingClient } = require('./common/messaging');
const { SotaRobot } = require('./robots');

const {
    ch_notification: COMMAND_TYPE,
} = require('../../../shared/messaging.json');

class NotificationService {
    constructor(
        msgServers,
        msgChannel,
        sotaRobotConfig,
        renewSotaRobotInterval = 3000,
    ) {
        this._msgServers = msgServers;
        this._msgChannel = msgChannel;
        this._sotaRobotConfig = sotaRobotConfig;
        this._renewSotaRobotInterval = renewSotaRobotInterval;

        this._msgCli = new MessagingClient(msgServers);
        this._sotaRobot = undefined;
        this._sotaRobotIp = undefined;

        this._handleNotifRequest = this._handleNotifRequest.bind(this);
        this._getNotificationSettings = this._getNotificationSettings.bind(
            this,
        );
        this._createSotaRobotInstance = this._createSotaRobotInstance.bind(
            this,
        );
        this._renewSotaRobotConnection = this._renewSotaRobotConnection.bind(
            this,
        );
    }

    async _handleNotifRequest(request) {
        console.debug(`Got a notification request: ${JSON.stringify(request)}`);

        const { command, params } = request;

        try {
            switch (command) {
                case COMMAND_TYPE.SOTA_ROBOT:
                    if (!this._sotaRobot) {
                        console.warn(
                            'Sota Robot command recieved, but connection to the robot is not established yet',
                        );
                    } else {
                        await this._sotaRobot.say(params.text);
                    }

                    break;
                default:
                    throw new Error(`Non-supported command type: ${command}`);
            }
        } catch (err) {
            console.error(err);
        }
    }

    async _getNotificationSettings() {
        const result = await models.systems.findById('notification');

        if (!result) {
            return undefined;
        } else {
            return result.content;
        }
    }

    _createSotaRobotInstance(ipAddress) {
        return new SotaRobot(
            this._sotaRobotConfig.authUrl,
            this._sotaRobotConfig.apiUrlTemplate.replace(
                '{IP_ADDRESS}',
                ipAddress,
            ),
            this._sotaRobotConfig.appId,
            this._sotaRobotConfig.apiKey,
        );
    }

    async _renewSotaRobotConnection() {
        try {
            const settings = await this._getNotificationSettings();

            if (!settings) {
                return;
            }

            const { ipAddress } = settings.sotaRobot;

            if (ipAddress !== this._sotaRobotIp) {
                const oldSotaRobot = this._sotaRobot;

                this._sotaRobot = undefined;
                this._sotaRobotIp = ipAddress;

                if (oldSotaRobot) {
                    await oldSotaRobot.disconnect();
                }

                if (ipAddress) {
                    try {
                        const sotaRobot = this._createSotaRobotInstance(
                            ipAddress,
                        );

                        await sotaRobot.connect();

                        this._sotaRobot = sotaRobot;
                    } catch (err) {
                        this._sotaRobot = undefined;

                        throw err;
                    }
                }

                console.log(
                    `Success to renew Sota Robot connection, ip address = ${ipAddress}`,
                );
            } else if (!this._sotaRobot && this._sotaRobotIp) {
                const sotaRobot = this._createSotaRobotInstance(ipAddress);

                await sotaRobot.connect();

                this._sotaRobot = sotaRobot;

                console.log(
                    `Success to renew Sota Robot connection, ip address = ${
                        this._sotaRobotIp
                    }`,
                );
            }
        } catch (err) {
            console.error(err);
        }
    }

    async start() {
        this._msgCli.connect();

        (async function renew() {
            await this._renewSotaRobotConnection();

            setTimeout(renew.bind(this), this._renewSotaRobotInterval);
        }.bind(this)());

        this._msgCli.subscribe(this._msgChannel, this._handleNotifRequest);
    }
}

module.exports = NotificationService;
