const NotificationService = require('./NotificationService');
const config = require('./config');
const msgConfig = require('../../../shared/messaging.json');

// TODO(feabries su): Read messaging configurations from a file, instead of hard
// coding.
const MESSAGING_SERVERS = ['nats://localhost:4222'];

new NotificationService(
    MESSAGING_SERVERS,
    msgConfig.channels.NOTIFICATION,
    // TODO(feabries su): Read Sota Robot configurations from database,
    // instead of hard coding.
    config.sotaRobot,
).start();
