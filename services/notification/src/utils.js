const uuidv4 = require('uuid/v4');

function genUUID() {
    return uuidv4();
}

module.exports = {
    genUUID,
};
