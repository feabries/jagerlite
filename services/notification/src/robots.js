const axios = require('axios');

const { genUUID } = require('./utils');

class Robot {
    connect() {
        throw new Error('The method is not implemeted yet.');
    }

    disconnect() {
        throw new Error('The method is not implemeted yet.');
    }

    say(text) {
        throw new Error('The method is not implemeted yet.');
    }
}

class SotaRobot extends Robot {
    constructor(authUrl, apiUrl, appId, apiKey) {
        super();

        this._authUrl = authUrl;
        this._apiUrl = apiUrl;
        this._appId = appId;
        this._apiKey = apiKey;

        this._token = undefined;
        this._connected = false;
    }

    async _post(
        url,
        { token = undefined, contentType = 'application/json', payload = {} },
    ) {
        const headers = {
            'Content-Type': contentType,
        };

        if (token) {
            headers['x-access-token'] = token;
        }

        const res = await axios.post(url, payload, {
            headers: headers,
            validateStatus: status => status < 400,
        });

        return res.data;
    }

    async _genAuthcode(uuid) {
        const data = await this._post(this._authUrl, {
            payload: {
                appid: this._appId,
                apikey: this._apiKey,
                uuid,
            },
        });

        return data.authcode;
    }

    async _genToken() {
        const uuid = genUUID();
        const authcode = await this._genAuthcode(uuid);

        const data = await this._post(`${this._apiUrl}/auth`, {
            payload: {
                authcode: authcode,
                uuid: uuid,
            },
        });

        return data.token;
    }

    async _overrideControl(override) {
        await this._post(this._apiUrl, {
            token: this._token,
            payload: {
                command: 'override_control',
                override_control: override,
            },
        });
    }

    async _say(text, model, motion) {
        await this._post(this._apiUrl, {
            token: this._token,
            payload: {
                command: 'say_stop',
            },
        });
        await this._post(this._apiUrl, {
            token: this._token,
            payload: {
                command: 'say',
                say: {
                    text,
                    model,
                    motion,
                    is_sync: false,
                },
            },
        });
    }

    async connect() {
        if (!this._connected) {
            this._token = await this._genToken();
            await this._overrideControl(true);

            this._connected = true;
        }
    }

    async disconnect() {
        if (this._connected) {
            this._connected = false;

            this._token = undefined;
            // FIXME(feabries su): The API call of override_control (false)
            // will return 400, which is not the same as Sota Robot's API
            // document. Please uncomment the following code and fix it.
            // await this._overrideControl(false);
        }
    }

    async say(text, model = 'Sota_A', motion = 'greeting') {
        await this._say(text, model, motion);
    }
}

module.exports = {
    Robot,
    SotaRobot,
};
