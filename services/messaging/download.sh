#!/bin/bash

readonly VERSION=v1.4.1

# TODO(feabries su): Support more architectures.
readonly -A ARCH_TABLE=(
    ["x86_64"]="amd64"
    ["aarch64"]="arm64"
)

messaging:download() {
    local -r arch=${1}
    local -r version=${2}

    local -r folder_name="gnatsd-${version}-linux-${arch}"
    local -r file_name="${folder_name}.zip"
    local -r download_path="https://github.com/nats-io/nats-server/releases/download/${version}/${file_name}"

    # Download the zip file and extract it.
    wget "${download_path}" -O "${file_name}"
    unzip -o "${file_name}"

    # Copy the executable binary.
    cp "${folder_name}/gnatsd" .

    # Remove the zip file and extracted folder.
    rm -rf "${file_name}" "${folder_name}"

    echo "The executable file is 'gnatsd'"
}

messaging:download "${ARCH_TABLE[$(uname -m)]}" "${VERSION}"
