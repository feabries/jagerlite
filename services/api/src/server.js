const app = require('./app');

// TODO(feabries su): Read from the configuration file instead of hard coding.
const PORT = 5000;

app.listen(PORT, err => {
    if (err) {
        console.error(err);
    } else {
        console.info(`==> Listening on port ${PORT}`);
    }
});
