const models = require('../models');

async function getSystemRecord(id) {
    return await models.systems.findById(id);
}

async function updateSystemRecord(
    id,
    content,
    condition = null,
    upsert = true,
) {
    const search = { _id: id };
    const updated = { content };
    const options = {
        upsert,
        runValidators: true,
    };

    if (condition) {
        search.content = condition;
    }

    return await models.systems.updateOne(search, updated, options);
}

async function getNetworkingContent() {
    const result = await getSystemRecord('networking');

    if (!result) {
        return undefined;
    } else {
        return result.content;
    }
}

async function updateNetworkingContent(network) {
    await updateSystemRecord('networking', network);
}

async function getNotificationContent() {
    const result = await getSystemRecord('notification');

    if (!result) {
        return undefined;
    } else {
        return result.content;
    }
}

async function updateNotificationContent(notification) {
    await updateSystemRecord('notification', notification);
}

module.exports = {
    getSystemRecord,
    updateSystemRecord,
    getNetworkingContent,
    updateNetworkingContent,
    getNotificationContent,
    updateNotificationContent,
};
