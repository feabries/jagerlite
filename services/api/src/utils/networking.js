const assert = require('assert');
const os = require('os');

const format = require('util').format;
const Promise = require('bluebird');

const includes = require('lodash/includes');
const merge = require('lodash/merge');
const values = require('lodash/values');

const execAsync = Promise.promisify(require('child_process').exec);

const {
    getNetworkingContent,
    updateNetworkingContent,
} = require('../utils/systems');
const { NETWORKING_MODE, NETWORKING_STATUS } = require('../constants');

function getInterfaceIp(interfaceName) {
    const interfaces = os.networkInterfaces();

    return interfaces[interfaceName][0].address;
}

async function setupWifi(ssid, password, timeout) {
    // Turn on wifi.
    await execAsync(format('nmcli radio wifi on'), { timeout });
    // Configure the wifi settings.
    // TODO(feabries su): Interface name is not necessary for wifi connection via
    // nmcli. It may make touble when multiple wifi interfaces exist.
    try {
        await execAsync(
            format(`nmcli device wifi connect ${ssid} password ${password}`),
            { timeout },
        );
    } catch (err) {
        const e = new Error(`Failed to connected to ${ssid}`);

        e.original = err;
        e.stack =
            e.stack
                .split('\n')
                .slice(0, 2)
                .join('\n') +
            '\n' +
            err.stack;

        throw e;
    }
}

async function setupNetworking(interfaceName, mode, config, timeout = 30000) {
    assert(
        includes(values(NETWORKING_MODE), mode),
        `Non-Supported networking mode: ${mode}`,
    );

    const baseSettings = {
        mode,
        config,
        ip: {
            address: '',
        },
        message: '',
    };

    try {
        if (mode === NETWORKING_MODE.WIFI) {
            const { ssid, password } = config;

            if (!ssid || !password) {
                // If ssid or password is not provided, do nothing.
                await updateNetworkingContent(
                    merge(baseSettings, {
                        status: NETWORKING_STATUS.DONE,
                    }),
                );
            } else {
                await updateNetworkingContent(
                    merge(baseSettings, {
                        status: NETWORKING_STATUS.PROCESSING,
                    }),
                );
                await setupWifi(ssid, password, timeout);
                await updateNetworkingContent(
                    merge(baseSettings, {
                        ip: {
                            address: getInterfaceIp(interfaceName),
                        },
                        status: NETWORKING_STATUS.DONE,
                    }),
                );
            }
        }
    } catch (err) {
        await updateNetworkingContent(
            merge(baseSettings, {
                status: NETWORKING_STATUS.FAILED,
                message: err.message,
            }),
        );

        throw err;
    }
}

module.exports = {
    setupNetworking,
};
