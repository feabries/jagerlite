const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');

const systems = require('./routes/systems');

const initNetworking = require('./init/networking');
const initNotificationSettings = require('./init/notification');

const app = express();

app.use(
    cors({
        optionsSuccessStatus: 200 /* some legacy browsers (IE11, various SmartTVs) choke on 204 */,
    }),
);
app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(bodyParser.urlencoded({ extended: false }));

// Initialize API.
// TODO(feabries su): Read from the configuration instaed of hard coding.
const API_ENTRY = '/api/v1';
app.use(API_ENTRY, systems);

// Logging errors.
app.use((err, req, res, next) => {
    console.error(err);
    next(err);
});

// Catch-all error handling.
app.use((err, req, res, next) => {
    const error = {
        code: err.code,
        message: err.message,
    };
    res.status(err.status).send({ error });
});

// Initialize networking.
initNetworking();

// Initialize notification settings.
initNotificationSettings();

module.exports = app;
