const express = require('express');

const includes = require('lodash/includes');
const isPlainObject = require('lodash/isPlainObject');
const isString = require('lodash/isString');
const values = require('lodash/values');

const {
    getNetworkingContent,
    getNotificationContent,
    updateNotificationContent,
} = require('../utils/systems');
const { setupNetworking } = require('../utils/networking');
const { createError } = require('../utils/common');
const { NETWORKING_MODE, NETWORKING_INTERFACE } = require('../constants');

const router = express.Router();

async function getNetworking(req, res, next) {
    try {
        const result = await getNetworkingContent();

        return res.status(200).send(result);
    } catch (err) {
        return next(createError(500, null, err));
    }
}

function updateNetworkingValidation(req, res, next) {
    // TODO(feabries su): Use a third-party library to validate.
    const { mode, config } = req.body;

    if (!includes(values(NETWORKING_MODE), mode)) {
        return next(createError(400, `Non-Supported networking mode: ${mode}`));
    }

    if (!isPlainObject(config)) {
        return next(createError(400, 'Config must be an object'));
    }

    if (mode === NETWORKING_MODE.WIFI) {
        if (!isString(config.ssid) || config.ssid.length === 0) {
            return next(
                createError(400, 'Wifi ssid must be a string whose length > 0'),
            );
        }
        if (!isString(config.password) || config.password.length === 0) {
            return next(
                createError(
                    400,
                    'Wifi password must be a string whose length > 0',
                ),
            );
        }
    }

    return next();
}

async function updateNetworking(req, res, next) {
    res.status(204).send();

    try {
        const { mode, config } = req.body;

        await setupNetworking(NETWORKING_INTERFACE, mode, config);
    } catch (err) {
        console.log(err);
    }
}

async function getNotificationSettings(req, res, next) {
    try {
        const result = await getNotificationContent();

        return res.status(200).send(result);
    } catch (err) {
        return next(createError(500, null, err));
    }
}

async function updateNotificationSettingsValidation(req, res, next) {
    // TODO(feabries su): Use a third-party library to validate.
    const { sotaRobot } = req.body;

    if (sotaRobot) {
        if (!isPlainObject(sotaRobot)) {
            return next(
                createError(400, 'Settings of Sota Robot must be an object'),
            );
        }

        if (
            !isString(sotaRobot.ipAddress) ||
            sotaRobot.ipAddress.length === 0
        ) {
            return next(
                createError(
                    400,
                    'Sota Robot IP address must be a string whose length > 0',
                ),
            );
        }
    }

    return next();
}

async function updateNotificationSettings(req, res, next) {
    try {
        const { sotaRobot } = req.body;
        const settings = await getNotificationContent();

        if (sotaRobot) {
            settings.sotaRobot.ipAddress = sotaRobot.ipAddress;
        }

        await updateNotificationContent(settings);

        return res.status(204).send();
    } catch (err) {
        return next(createError(500, null, err));
    }
}

router.get('/system/networking', getNetworking);
router.patch(
    '/system/networking',
    updateNetworkingValidation,
    updateNetworking,
);

router.get('/system/notification', getNotificationSettings);
router.patch(
    '/system/notification',
    updateNotificationSettingsValidation,
    updateNotificationSettings,
);

module.exports = router;
