const NETWORKING_MODE = Object.freeze({
    WIFI: 'wifi',
});

const NETWORKING_STATUS = Object.freeze({
    PROCESSING: 'processing',
    DONE: 'done',
    FAILED: 'failed',
});

// TODO(feabries su): Read from the configuration file, instead of hard coding.
const NETWORKING_INTERFACE = 'wlan0';

module.exports = {
    NETWORKING_MODE,
    NETWORKING_STATUS,
    NETWORKING_INTERFACE,
};
