const { loadModels, DatabaseClient } = require('./common/database');

const DATABASE_SCHEMA_URL = '../../shared/database.json';

// TODO(feabries su): Read from the configuration file instead of hard coding.
const DATABASE_URL = `mongodb://jager:${encodeURIComponent(
    'jager@0402!prod',
)}@localhost:21034`;
const DATABASE_NAME = 'jager_production';

const dbCli = new DatabaseClient(DATABASE_URL, DATABASE_NAME, 'admin');
dbCli.connect();

const models = loadModels(DATABASE_SCHEMA_URL, dbCli);

module.exports = models;
