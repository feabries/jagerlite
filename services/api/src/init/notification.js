const {
    getNotificationContent,
    updateNotificationContent,
} = require('../utils/systems');

async function initNotificationSettings() {
    try {
        const settings = await getNotificationContent();

        if (!settings) {
            await updateNotificationContent({
                sotaRobot: {
                    ipAddress: '',
                },
            });
        }
    } catch (err) {
        console.error(err);

        // TODO(feabries su): More error handling.
    }
}

module.exports = initNotificationSettings;
