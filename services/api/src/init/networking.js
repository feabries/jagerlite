const { getNetworkingContent } = require('../utils/systems');
const { setupNetworking } = require('../utils/networking');
const {
    NETWORKING_MODE,
    NETWORKING_STATUS,
    NETWORKING_INTERFACE,
} = require('../constants');

async function initNetworking(retry = true, retryAfter = 3000) {
    try {
        const settings = await getNetworkingContent();
        const mode = settings ? settings.mode : NETWORKING_MODE.WIFI;
        const config = settings
            ? settings.config
            : {
                  ssid: '',
                  password: '',
              };

        await setupNetworking(NETWORKING_INTERFACE, mode, config);
    } catch (err) {
        console.error(err);

        if (retry) {
            setTimeout(async () => {
                console.log('retry');
                await initNetworking(retry, retryAfter);
            }, retryAfter);
        }
    }
}

module.exports = initNetworking;
