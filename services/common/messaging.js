const assert = require('assert');

const NATS = require('nats');

class MessagingClient {
    constructor(servers) {
        this._servers = servers;

        this._cli = undefined;
    }

    _connect(servers) {
        // TODO(feabries su): More flexible options for connection.
        const cli = NATS.connect({
            json: true,
            maxReconnectAttempts: -1,
            reconnectTimeWait: 250,
            servers,
        });

        // TODO(feabries su): More flexible options for events handling.
        cli.on('error', err => {
            console.error(err);
        });
        cli.on('connect', nc => {
            console.log('Messaging connected');
        });
        cli.on('disconnect', () => {
            console.log('Messaging disconnected');
        });
        cli.on('reconnecting', () => {
            console.log('Messaging reconnecting');
        });
        cli.on('close', () => {
            console.log('Messaging connection closed');
        });

        return cli;
    }

    _check_connected() {
        assert(this._cli, 'Messaging is not connected.');
    }

    _check_disconnected() {
        assert(!this._cli, 'Messaging is connected.');
    }

    connect() {
        this._check_disconnected();

        this._cli = this._connect(this._servers);
    }

    disconnect() {
        this._check_connected();

        this._cli.close();
        this._cli = undefined;
    }

    subscribe(channel, handler) {
        this._check_connected();

        this._cli.subscribe(channel, handler);
    }

    // TODO(feabries su): There are more and more messaging methods, please
    // implement them when they are necessary.
}

module.exports = {
    MessagingClient,
};
