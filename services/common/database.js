const assert = require('assert');
const fs = require('fs');
const JSONPath = require('JSONPath');

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Use bluebird for Mongoose
mongoose.Promise = require('bluebird');

class DatabaseClient {
    constructor(url, dbName, authSource) {
        this._url = url;
        this._dbName = dbName;
        this._authSource = authSource;

        this.connection = undefined;
    }

    _connect(url, dbName, authSource) {
        // TODO(feabries su): More flexible options for connection.
        const dbUrl = `${url}/${dbName}`;
        const options = {
            auth: { authSource },
            useNewUrlParser: true,
            reconnectTries: Number.MAX_VALUE, // Never stop trying to redbect
            reconnectInterval: 500, // Reconnect every 500ms
        };
        /*
        const cli = mongoose.connect(dbUrl, options);
        */
        const cli = mongoose.createConnection(dbUrl, options);

        // TODO(feabries su): More flexible options for events handling.
        cli.on('connecting', () => {
            console.info('Database connecting');
        });
        cli.on('error', err => {
            console.error(err);
            // Handle the case for failure on first dbection.
            if (
                err.message &&
                err.message.match(
                    /failed to connect to server .* on first connect/,
                )
            ) {
                setTimeout(() => {
                    console.info('Retrying first connect to database server');
                    cli.openUri(dbUrl, options).catch(() => {});
                }, 3 * 1000);
            }
        });
        cli.on('connected', () => {
            console.info('Database connected');
        });
        cli.once('open', () => {
            console.info('Database opened');
        });
        cli.on('reconnected', () => {
            console.info('Database reconnected');
        });
        cli.on('disconnecting', () => {
            console.info('Database disconnecting');
        });
        cli.on('disconnected', () => {
            console.info('Database disconnected');
        });

        return cli;
    }

    _check_connected() {
        assert(this.connection, 'Messaging is not connected.');
    }

    _check_disconnected() {
        assert(!this.connection, 'Messaging is connected.');
    }

    connect() {
        this._check_disconnected();

        this.connection = this._connect(
            this._url,
            this._dbName,
            this._authSource,
        );
    }

    disconnect() {
        this._check_connected();

        this.connection.disconnect();
        this.connection = undefined;
    }
}

function createSchema(schemaRep, options = {}) {
    const schemaObj = new Schema(schemaRep, options);

    return schemaObj;
}

function importSchema(schemaUrl) {
    const schemaJSON = JSON.parse(fs.readFileSync(schemaUrl, 'utf8'));
    const mainDocSchemaList = Object.keys(schemaJSON).filter(key => {
        return !key.startsWith('subdoc_');
    });
    const subDocSchemaList = Object.keys(schemaJSON).filter(key => {
        return key.startsWith('subdoc_');
    });
    const mainDocSchemaObj = {};
    const subDocSchemaObj = {};

    // Import subdocument schema first
    subDocSchemaList.forEach(item => {
        // Validate schema
        const result = JSONPath({
            json: schemaJSON[item],
            path: '$..*@string()',
        }).filter(value => {
            return value.startsWith('SUBDOC_');
        });
        if (result && result.length > 0) {
            throw new Error('Subdocument of subdocument is not allow');
        }

        // Create subdocument object
        subDocSchemaObj[item] = createSchema(schemaJSON[item]);
    });

    // For each main document schema, replace the subdocument placeholder
    // with real subdocument object and then create the schema object
    mainDocSchemaList.forEach(item => {
        const propertyList = JSONPath({
            json: schemaJSON[item],
            path: '$..*@string()',
            resultType: 'all',
        });
        const subDocPropertyList = propertyList.filter(property => {
            return property.value.startsWith('SUBDOC_');
        });
        subDocPropertyList.forEach(property => {
            // Find matching subdocument schema object
            const match = Object.keys(subDocSchemaObj).find(obj => {
                return obj.toUpperCase() === property.value;
            });
            if (!match) {
                throw new Error(
                    `Unable to find definition for subdocument placeholder "${
                        property.value
                    }" in main document schema "${item}"`,
                );
            }

            // Replace subdocument placeholder with matched subdocument object
            const pathArray = JSONPath.toPathArray(property.path);
            const parentProperty = pathArray
                .slice(1, -1)
                .reduce((accu, curr) => {
                    return accu ? accu[curr] : null;
                }, schemaJSON[item]);
            parentProperty[pathArray.pop()] = subDocSchemaObj[match];
        });
        // Create main document object
        mainDocSchemaObj[item] = createSchema(schemaJSON[item]);
    });

    return mainDocSchemaObj;
}

function loadModels(schemaUrl, dbCli) {
    const databaseSchemaObj = importSchema(schemaUrl, dbCli);

    const models = {};
    Object.keys(databaseSchemaObj).forEach(objName => {
        models[objName] = dbCli.connection.model(
            objName,
            databaseSchemaObj[objName],
            objName,
        );
    });

    return models;
}

module.exports = {
    DatabaseClient,
    loadModels,
};
